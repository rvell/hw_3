import java.util.EmptyStackException;
import java.util.LinkedList;
import java.util.Iterator;

public class DoubleStack {

    /*
   The following code snippets are derived *currently* directly from
   the same subjects LongStack task of Autumn 2016. And are to be arranged
   into DoubleStack format by 15.10.17
    */
    /*
    Algne Õppejõu poolne tagasiside -
    Meetod interpret ei töötle vigast avaldist piisavalt arusaadavalt -
    kasutajale ei kuvata vea korral (näiteks vigane sümbol avaldises või
    liiga vara esinenud tehtemärk) kogu vajalikku infot (näiteks milline oli
    vigane string tervikuna ning milles täpsemalt seisnes viga).
   */
    private final LinkedList<Double> stack;

   public static void main (String[] argum) {
      double a = interpret("1");
      //double b = interpret("1 2");
      /* Error message for lack of symbols. */
      double c = interpret("1 2 - ");
      double d = interpret("1 2 3 - + ");
      //double e = interpret("1 - 2");
      /* Exception for an early symbol in reverse polish notation */
      //double f = interpret("1 2 # ");
      /* Exception from op is handled by try-catch block, which throws a more
         accurate and understandable error message.*/

      System.out.println(interpret("3 -"));

   }

   DoubleStack() {
      this.stack = new LinkedList<>();
   }

   DoubleStack(LinkedList <Double> stack){
       this.stack = stack;
   }


   @Override
   public Object clone() throws CloneNotSupportedException {
      return new DoubleStack((LinkedList<Double>) this.stack.clone());
   }

   //Stacki alatäitumise kontroll
   public boolean stEmpty() {
      return this.stack.isEmpty();
   }

   //Stacki lisamine
   public void push (double a) {
      this.stack.push(a);
   }

   //Stackist eemaldamine
   public double pop() {
       if(!stack.isEmpty()){
           return this.stack.pop();
       }
       else {
           throw new EmptyStackException();
       }
   }


   public void op (String s) {

       if(s.equals(" ") | s.equals("\t")){
           return;
       }
       if(s.equals("+")){
             this.stack.push(this.stack.pop() + this.stack.pop());

      }
      else if(s.equals("-")){
          double a = this.pop();
          double b = this.pop();
          this.stack.push(b-a);
      }
      else if(s.equals("*")){
          this.stack.push(this.stack.pop() * this.stack.pop());
      }
      else if(s.equals("/")){
          double a = this.pop();
          double b = this.pop();
          this.stack.push(b/a);
      }
      else{
          throw new RuntimeException("Invalid Operator: " + s + " for: " + this.stack);
      }

      if (stEmpty()){
           throw new EmptyStackException();
       }
   }
  
   public double tos() {
      if(!stack.isEmpty()){
          return this.stack.peek();
      }
      else{
          throw new EmptyStackException();
      }

   }

   @Override
   public boolean equals (Object o) {
      return this.stack.equals(((DoubleStack)o).stack);
   }

   @Override
   public String toString() {
       StringBuilder stringb = new StringBuilder();
       Iterator<Double> i = this.stack.descendingIterator();
         while(i.hasNext()){
            stringb.append(i.next());
            stringb.append(" ");
         }
      return stringb.toString();
   }

    // Interpret class is based on Artis Zirk's solution adapted
    // from https://git.wut.ee/i231/home3/src/master/src/LongStack.java

   public static double interpret (String pol) {
      DoubleStack doublestack = new DoubleStack();
      for (int i = 0; i < pol.length(); i++){
          char c = pol.charAt(i);
          char nc = ' ';
          if (i+1 < pol.length()){
              nc = pol.charAt(i+1);
          }
          if((c == '-' & nc >= '0' & nc <= '9') | (c >= '0' & c <= '9')){
              StringBuilder buffer = new StringBuilder();
              buffer.append(c);
              i++;
              for(; i<pol.length(); i++){
                  c = pol.charAt(i);
                  if(c >= '0' & c <= '9'){
                      buffer.append(c);
                  }
                  else {
                      break;
                  }
              }
              doublestack.push(Double.parseDouble(buffer.toString()));
          }
          else try {
              doublestack.op("" + c);
          }
          catch (RuntimeException e) {
              throw new RuntimeException("Invalid Operator: "+ c
                      + " Or operator placement for reverse polish notion: "  + pol);
          }
      }
      if (doublestack.stack.size() == 1){
          return doublestack.pop();
      }
      else {
          throw new RuntimeException("The following input isn't in reverse polish notion: " + pol
                  + " <- Probably lacks symbols.");
      }
   }

}

